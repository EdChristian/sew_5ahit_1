﻿namespace Spiel_Client
{
    partial class Client
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.field33 = new System.Windows.Forms.Label();
            this.field32 = new System.Windows.Forms.Label();
            this.field31 = new System.Windows.Forms.Label();
            this.field23 = new System.Windows.Forms.Label();
            this.field22 = new System.Windows.Forms.Label();
            this.field21 = new System.Windows.Forms.Label();
            this.field13 = new System.Windows.Forms.Label();
            this.field12 = new System.Windows.Forms.Label();
            this.field11 = new System.Windows.Forms.Label();
            this.head = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // field33
            // 
            this.field33.AutoSize = true;
            this.field33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.field33.Location = new System.Drawing.Point(144, 186);
            this.field33.MinimumSize = new System.Drawing.Size(60, 60);
            this.field33.Name = "field33";
            this.field33.Size = new System.Drawing.Size(60, 60);
            this.field33.TabIndex = 97;
            this.field33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.field33.Click += new System.EventHandler(this.field33_Click);
            // 
            // field32
            // 
            this.field32.AutoSize = true;
            this.field32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.field32.Location = new System.Drawing.Point(78, 186);
            this.field32.MinimumSize = new System.Drawing.Size(60, 60);
            this.field32.Name = "field32";
            this.field32.Size = new System.Drawing.Size(60, 60);
            this.field32.TabIndex = 96;
            this.field32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.field32.Click += new System.EventHandler(this.field32_Click);
            // 
            // field31
            // 
            this.field31.AutoSize = true;
            this.field31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.field31.Location = new System.Drawing.Point(12, 186);
            this.field31.MinimumSize = new System.Drawing.Size(60, 60);
            this.field31.Name = "field31";
            this.field31.Size = new System.Drawing.Size(60, 60);
            this.field31.TabIndex = 95;
            this.field31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.field31.Click += new System.EventHandler(this.field31_Click);
            // 
            // field23
            // 
            this.field23.AutoSize = true;
            this.field23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.field23.Location = new System.Drawing.Point(144, 116);
            this.field23.MinimumSize = new System.Drawing.Size(60, 60);
            this.field23.Name = "field23";
            this.field23.Size = new System.Drawing.Size(60, 60);
            this.field23.TabIndex = 94;
            this.field23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.field23.Click += new System.EventHandler(this.field23_Click);
            // 
            // field22
            // 
            this.field22.AutoSize = true;
            this.field22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.field22.Location = new System.Drawing.Point(78, 116);
            this.field22.MinimumSize = new System.Drawing.Size(60, 60);
            this.field22.Name = "field22";
            this.field22.Size = new System.Drawing.Size(60, 60);
            this.field22.TabIndex = 93;
            this.field22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.field22.Click += new System.EventHandler(this.field22_Click);
            // 
            // field21
            // 
            this.field21.AutoSize = true;
            this.field21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.field21.Location = new System.Drawing.Point(12, 116);
            this.field21.MinimumSize = new System.Drawing.Size(60, 60);
            this.field21.Name = "field21";
            this.field21.Size = new System.Drawing.Size(60, 60);
            this.field21.TabIndex = 92;
            this.field21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.field21.Click += new System.EventHandler(this.field21_Click);
            // 
            // field13
            // 
            this.field13.AutoSize = true;
            this.field13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.field13.Location = new System.Drawing.Point(144, 46);
            this.field13.MinimumSize = new System.Drawing.Size(60, 60);
            this.field13.Name = "field13";
            this.field13.Size = new System.Drawing.Size(60, 60);
            this.field13.TabIndex = 91;
            this.field13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.field13.Click += new System.EventHandler(this.field13_Click);
            // 
            // field12
            // 
            this.field12.AutoSize = true;
            this.field12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.field12.Location = new System.Drawing.Point(78, 46);
            this.field12.MinimumSize = new System.Drawing.Size(60, 60);
            this.field12.Name = "field12";
            this.field12.Size = new System.Drawing.Size(60, 60);
            this.field12.TabIndex = 90;
            this.field12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.field12.Click += new System.EventHandler(this.field12_Click);
            // 
            // field11
            // 
            this.field11.AutoSize = true;
            this.field11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.field11.Location = new System.Drawing.Point(12, 46);
            this.field11.MinimumSize = new System.Drawing.Size(60, 60);
            this.field11.Name = "field11";
            this.field11.Size = new System.Drawing.Size(60, 60);
            this.field11.TabIndex = 89;
            this.field11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.field11.Click += new System.EventHandler(this.field11_Click);
            // 
            // head
            // 
            this.head.AutoSize = true;
            this.head.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.head.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.head.Location = new System.Drawing.Point(49, 9);
            this.head.MinimumSize = new System.Drawing.Size(124, 0);
            this.head.Name = "head";
            this.head.Size = new System.Drawing.Size(129, 24);
            this.head.TabIndex = 88;
            this.head.Text = "Spielspass (:";
            this.head.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(220, 263);
            this.Controls.Add(this.field33);
            this.Controls.Add(this.field32);
            this.Controls.Add(this.field31);
            this.Controls.Add(this.field23);
            this.Controls.Add(this.field22);
            this.Controls.Add(this.field21);
            this.Controls.Add(this.field13);
            this.Controls.Add(this.field12);
            this.Controls.Add(this.field11);
            this.Controls.Add(this.head);
            this.Name = "Client";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label field33;
        private System.Windows.Forms.Label field32;
        private System.Windows.Forms.Label field31;
        private System.Windows.Forms.Label field23;
        private System.Windows.Forms.Label field22;
        private System.Windows.Forms.Label field21;
        private System.Windows.Forms.Label field13;
        private System.Windows.Forms.Label field12;
        private System.Windows.Forms.Label field11;
        private System.Windows.Forms.Label head;
    }
}

