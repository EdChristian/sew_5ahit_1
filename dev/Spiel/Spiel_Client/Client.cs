﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Spiel_Client
{
    public partial class Client : Form
    {
        int player = 0;

        static TcpClient client = new TcpClient("127.0.0.1", 2055);
        StreamReader sr = new StreamReader(client.GetStream());
        StreamWriter sw = new StreamWriter(client.GetStream())
        {
            AutoFlush = true,
        };

        public Client()
        {
            InitializeComponent();
            string player_string = sr.ReadLine();
            player = Convert.ToInt32(player_string.Split(' ')[1]);
            MessageBox.Show("Connected as " + player_string);
        }

        public void Draw()
        {
            string received = sr.ReadLine();
            string[] received_split = received.Split('/');
            string split_player = received_split[0];
            string split_label = received_split[1];

            if (!split_player.Equals(player.ToString()))
            {
            }
            else
            {
                #region playerO
                if (split_player == "0")
                {
                    player = Convert.ToInt32(split_player);

                    if (split_label == "11")
                    {
                        field11.Text = "O";
                    }
                    if (split_label == "12")
                    {
                        field12.Text = "O";
                    }
                    if (split_label == "13")
                    {
                        field13.Text = "O";
                    }
                    if (split_label == "21")
                    {
                        field21.Text = "O";
                    }
                    if (split_label == "22")
                    {
                        field22.Text = "O";
                    }
                    if (split_label == "23")
                    {
                        field23.Text = "O";
                    }
                    if (split_label == "31")
                    {
                        field31.Text = "O";
                    }
                    if (split_label == "32")
                    {
                        field32.Text = "O";
                    }
                    if (split_label == "33")
                    {
                        field33.Text = "O";
                    }
                }
                #endregion 
                
                #region playerX
                if (split_player == "1")
                {
                    player = Convert.ToInt32(split_player);

                    if (split_label == "11")
                    {
                        field11.Text = "X";
                    }
                    if (split_label == "12")
                    {
                        field12.Text = "X";
                    }
                    if (split_label == "13")
                    {
                        field13.Text = "X";
                    }
                    if (split_label == "21")
                    {
                        field21.Text = "X";
                    }
                    if (split_label == "22")
                    {
                        field22.Text = "X";
                    }
                    if (split_label == "23")
                    {
                        field23.Text = "X";
                    }
                    if (split_label == "31")
                    {
                        field31.Text = "X";
                    }
                    if (split_label == "32")
                    {
                        field32.Text = "X";
                    }
                    if (split_label == "33")
                    {
                        field33.Text = "X";
                    }
                }
                #endregion
            }
        }

        private void field11_Click(object sender, EventArgs e)
        {
            if (field11.Text == "")
            {
                sw.WriteLine(player.ToString() + "/11");
                if (player == 0)
                    field11.Text = "X";
                else
                    field11.Text = "O";
                this.Refresh(); 
            }
            else
                MessageBox.Show("Field already used!");

            Draw();
            check();
        }

        private void field12_Click(object sender, EventArgs e)
        {
            if (field12.Text == "")
            {
                sw.WriteLine(player.ToString() + "/12");
                if (player == 0)
                    field12.Text = "X";
                else
                    field12.Text = "O";
                this.Refresh();
            }
            else
                MessageBox.Show("Field already used!");

            Draw();
            check();
        }

        private void field13_Click(object sender, EventArgs e)
        {
            if (field13.Text == "")
            {
                sw.WriteLine(player.ToString() + "/13");
                if (player == 0)
                    field13.Text = "X";
                else
                    field13.Text = "O";
                this.Refresh();
            }
            else
                MessageBox.Show("Field already used!");

            Draw();
            check();
        }

        private void field21_Click(object sender, EventArgs e)
        {
            if (field21.Text == "")
            {
                sw.WriteLine(player.ToString() + "/21");
                if (player == 0)
                    field21.Text = "X";
                else
                    field21.Text = "O";
                this.Refresh();
            }
            else
                MessageBox.Show("Field already used!");

            Draw();
            check();
        }

        private void field22_Click(object sender, EventArgs e)
        {
            if (field22.Text == "")
            {
                sw.WriteLine(player.ToString() + "/22");
                if (player == 0)
                    field22.Text = "X";
                else
                    field22.Text = "O";
                this.Refresh();
            }
            else
                MessageBox.Show("Field already used!");

            Draw();
            check();
        }

        private void field23_Click(object sender, EventArgs e)
        {
            if (field23.Text == "")
            {
                sw.WriteLine(player.ToString() + "/23");
                if (player == 0)
                    field23.Text = "X";
                else
                    field23.Text = "O";
                this.Refresh();
            }
            else
                MessageBox.Show("Field already used!");

            Draw();
            check();
        }

        private void field31_Click(object sender, EventArgs e)
        {
            if (field31.Text == "")
            {
                sw.WriteLine(player.ToString() + "/31");
                if (player == 0)
                    field31.Text = "X";
                else
                    field31.Text = "O";
                this.Refresh();
            }
            else
                MessageBox.Show("Field already used!");

            Draw();
            check();
        }

        private void field32_Click(object sender, EventArgs e)
        {
            if (field32.Text == "")
            {
                sw.WriteLine(player.ToString() + "/32");
                if (player == 0)
                    field32.Text = "X";
                else
                    field32.Text = "O";
                this.Refresh();
            }
            else
                MessageBox.Show("Field already used!");

            Draw();
            check();
        }

        private void field33_Click(object sender, EventArgs e)
        {
            if (field33.Text == "")
            {
                sw.WriteLine(player.ToString() + "/33");
                if (player == 0)
                    field33.Text = "X";
                else
                    field33.Text = "O";
                this.Refresh();
            }
            else
                MessageBox.Show("Field already used!");
            
            Draw();
            check();
        }

        public void check()
        {
            //1. Zeile
            if (field11.Text != "" & field12.Text != "" & field13.Text != "")
            {
                if (field11.Text == field12.Text && field12.Text == field13.Text)
                {
                    field11.BackColor = Color.LightGreen;
                    field12.BackColor = Color.LightGreen;
                    field13.BackColor = Color.LightGreen;

                    if (field11.Text == "X")
                    {
                        MessageBox.Show("Player 1 wins!");
                    }
                    else
                    {
                        MessageBox.Show("Player 2 wins!");
                    }
                }
            }

            //2. Zeile
            if (field21.Text != "" & field22.Text != "" & field23.Text != "")
            {
                if (field21.Text == field22.Text && field22.Text == field23.Text)
                {
                    field21.BackColor = field22.BackColor = field23.BackColor = Color.LightGreen;

                    if (field21.Text == "X")
                    {
                        MessageBox.Show("Player 1 wins!");
                    }
                    else
                    {
                        MessageBox.Show("Player 2 wins!");
                    }
                }
            }

            //3. Zeile
            if (field31.Text != "" & field32.Text != "" & field33.Text != "")
            {
                if (field31.Text == field32.Text && field32.Text == field33.Text)
                {
                    field31.BackColor = field32.BackColor = field33.BackColor = Color.LightGreen;

                    if (field31.Text == "X")
                    {
                        MessageBox.Show("Player 1 wins!");
                    }
                    else
                    {
                        MessageBox.Show("Player 2 wins!");
                    }
                }
            }

            //1. Spalte
            if (field11.Text != "" & field21.Text != "" & field31.Text != "")
            {
                if (field11.Text == field21.Text && field21.Text == field31.Text)
                {
                    field11.BackColor = field21.BackColor = field31.BackColor = Color.LightGreen;

                    if (field11.Text == "X")
                    {
                        MessageBox.Show("Player 1 wins!");
                    }
                    else
                    {
                        MessageBox.Show("Player 2 wins!");
                    }
                }
            }

            //2. Spalte
            if (field12.Text != "" & field22.Text != "" & field32.Text != "")
            {
                if (field12.Text == field22.Text && field22.Text == field32.Text)
                {
                    field12.BackColor = field22.BackColor = field32.BackColor = Color.LightGreen;

                    if (field12.Text == "X")
                    {
                        MessageBox.Show("Player 1 wins!");
                    }
                    else
                    {
                        MessageBox.Show("Player 2 wins!");
                    }
                }
            }

            //3. Spalte
            if (field13.Text != "" & field23.Text != "" & field33.Text != "")
            {
                if (field13.Text == field23.Text && field23.Text == field33.Text)
                {
                    field13.BackColor = field23.BackColor = field33.BackColor = Color.LightGreen;

                    if (field13.Text == "X")
                    {
                        MessageBox.Show("Player 1 wins!");
                    }
                    else
                    {
                        MessageBox.Show("Player 2 wins!");
                    }
                }
            }

            //Diagonale links oben nach rechts unten
            if (field11.Text != "" & field22.Text != "" & field33.Text != "")
            {
                if (field11.Text == field22.Text && field22.Text == field33.Text)
                {
                    field11.BackColor = field22.BackColor = field33.BackColor = Color.LightGreen;

                    if (field11.Text == "X")
                    {
                        MessageBox.Show("Player 1 wins!");
                    }
                    else
                    {
                        MessageBox.Show("Player 2 wins!");
                    }
                }
            }

            //Diagonale rechts oben nach links unten
            if (field13.Text != "" & field22.Text != "" & field31.Text != "")
            {
                if (field13.Text == field22.Text && field22.Text == field31.Text)
                {
                    field13.BackColor = field22.BackColor = field31.BackColor = Color.LightGreen;

                    if (field13.Text == "X")
                    {
                        MessageBox.Show("Player 1 wins!");
                    }
                    else
                    {
                        MessageBox.Show("Player 2 wins!");
                    }
                }
            }

            //Überprüfung volles Feld - kein Gewinner
            if (field11.Text != "" & field12.Text != "" & field13.Text != "" &
                field21.Text != "" & field22.Text != "" & field23.Text != "" &
                field31.Text != "" & field32.Text != "" & field33.Text != "")
            {
                MessageBox.Show("That's a draw!");
            }
        }
    }
}
