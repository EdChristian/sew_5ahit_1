﻿namespace Prototyp_dynamisch
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.textSize = new System.Windows.Forms.TextBox();
            this.size = new System.Windows.Forms.Label();
            this.okSize = new System.Windows.Forms.Button();
            this.playerTurn = new System.Windows.Forms.Label();
            this.playerLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textSize
            // 
            this.textSize.Location = new System.Drawing.Point(362, 31);
            this.textSize.Name = "textSize";
            this.textSize.Size = new System.Drawing.Size(100, 20);
            this.textSize.TabIndex = 0;
            // 
            // size
            // 
            this.size.AutoSize = true;
            this.size.Location = new System.Drawing.Point(362, 9);
            this.size.Name = "size";
            this.size.Size = new System.Drawing.Size(36, 13);
            this.size.TabIndex = 1;
            this.size.Text = "Größe";
            // 
            // okSize
            // 
            this.okSize.Location = new System.Drawing.Point(365, 58);
            this.okSize.Name = "okSize";
            this.okSize.Size = new System.Drawing.Size(75, 23);
            this.okSize.TabIndex = 2;
            this.okSize.Text = "OK";
            this.okSize.UseVisualStyleBackColor = true;
            this.okSize.Click += new System.EventHandler(this.okSize_Click);
            // 
            // playerTurn
            // 
            this.playerTurn.AutoSize = true;
            this.playerTurn.Location = new System.Drawing.Point(362, 139);
            this.playerTurn.Name = "playerTurn";
            this.playerTurn.Size = new System.Drawing.Size(39, 13);
            this.playerTurn.TabIndex = 3;
            this.playerTurn.Text = "Spieler";
            // 
            // playerLabel
            // 
            this.playerLabel.AutoSize = true;
            this.playerLabel.Location = new System.Drawing.Point(362, 165);
            this.playerLabel.Name = "playerLabel";
            this.playerLabel.Size = new System.Drawing.Size(35, 13);
            this.playerLabel.TabIndex = 4;
            this.playerLabel.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 321);
            this.Controls.Add(this.playerLabel);
            this.Controls.Add(this.playerTurn);
            this.Controls.Add(this.okSize);
            this.Controls.Add(this.size);
            this.Controls.Add(this.textSize);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textSize;
        private System.Windows.Forms.Label size;
        private System.Windows.Forms.Button okSize;
        private System.Windows.Forms.Label playerTurn;
        private System.Windows.Forms.Label playerLabel;
    }
}

