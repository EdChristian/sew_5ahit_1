﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prototyp_dynamisch
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public TableLayoutPanel tlPanel = new TableLayoutPanel();
        public Panel p;
        Logik l = new Logik();
        int playerRound;

        private void okSize_Click(object sender, EventArgs e)
        {
           playerRound = 1;

            this.tlPanel.Controls.Clear();
            tlPanel.Height=300;
            tlPanel.Width=300;
            int size = Convert.ToInt32(textSize.Text);

            tlPanel.ColumnCount = Convert.ToInt32(textSize.Text);
            tlPanel.RowCount = Convert.ToInt32(textSize.Text);

            for (int i = 0; i < size * size; i++)
            {
                tlPanel.Controls.Add(p = new Panel() { Height = tlPanel.Height / size -5, Width = tlPanel.Width / size -5});
                p.Click += new EventHandler(ClickPlace);
            }

            foreach (Control c in tlPanel.Controls)
            {
                c.BackColor = Color.Gray;
                this.Controls.Add(tlPanel);
            }
            displayTurn();
        }

        void ClickPlace(object sender, EventArgs e)
        {
            var position = tlPanel.GetPositionFromControl(sender as Panel);
            var control = tlPanel.GetControlFromPosition(position.Column, position.Row);

            displayTurn();

            switch (l.isClicked(position.Column, position.Row))
            {
                case 1:
                    if (control.BackColor == Color.Gray)
                    {
                        (sender as Panel).BackColor = Color.Goldenrod;
                        playerRound = 1;
                    }
                    //displayTurn();
                    break;
                case 2:
                    if (control.BackColor == Color.Gray)
                    {
                        (sender as Panel).BackColor = Color.CornflowerBlue;
                        playerRound = 2;
                    }
                    //displayTurn();
                    break;
            }
        }

        void displayTurn()
        {
            if (playerRound%2!=0)
                playerLabel.Text = "Player 1";
            else
                playerLabel.Text = "Player 2";
        }
    }
}
