﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class Form1 : Form
    {
        int player = 1;
        int score1 = 0;
        int score2 = 0;
        public Form1()
        {
            InitializeComponent();
            displayTurn();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "")
            {
                if (player == 1)
                {
                    button1.Text = "X";
                    player++;
                }
                else
                {
                    button1.Text = "O";
                    player--;
                }
            }
            else
                button1.Text = button1.Text;
            displayTurn();
            checkIt();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (button2.Text == "")
            {
                if (player == 1)
                {
                    button2.Text = "X";
                    player++;
                }
                else
                {
                    button2.Text = "O";
                    player--;
                }
            }
            else
                button2.Text = button2.Text;
            displayTurn();
            checkIt();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (button3.Text == "")
            {
                if (player == 1)
                {
                    button3.Text = "X";
                    player++;
                }
                else
                {
                    button3.Text = "O";
                    player--;
                }
            }
            else
                button3.Text = button3.Text;
            displayTurn();
            checkIt();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (button4.Text == "")
            {
                if (player == 1)
                {
                    button4.Text = "X";
                    player++;
                }
                else
                {
                    button4.Text = "O";
                    player--;
                }
            }
            else
                button4.Text = button4.Text;
            displayTurn();
            checkIt();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (button5.Text == "")
            {
                if (player == 1)
                {
                    button5.Text = "X";
                    player++;
                }
                else
                {
                    button5.Text = "O";
                    player--;
                }
            }
            else
                button5.Text = button5.Text;
            displayTurn();
            checkIt();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (button6.Text == "")
            {
                if (player == 1)
                {
                    button6.Text = "X";
                    player++;
                }
                else
                {
                    button6.Text = "O";
                    player--;
                }
            }
            else
                button6.Text = button6.Text;
            displayTurn();
            checkIt();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (button7.Text == "")
            {
                if (player == 1)
                {
                    button7.Text = "X";
                    player++;
                }
                else
                {
                    button7.Text = "O";
                    player--;
                }
            }
            else
                button7.Text = button7.Text;
            displayTurn();
            checkIt();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (button8.Text == "")
            {
                if (player == 1)
                {
                    button8.Text = "X";
                    player++;
                }
                else
                {
                    button8.Text = "O";
                    player--;
                }
            }
            else
                button8.Text = button8.Text;
            displayTurn();
            checkIt();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (button9.Text == "")
            {
                if (player == 1)
                {
                    button9.Text = "X";
                    player++;
                }
                else
                {
                    button9.Text = "O";
                    player--;
                }
            }
            else
                button9.Text = button9.Text;
            displayTurn();
            checkIt();
        }



        public void displayTurn()
        {
            if (player == 1)
                displayPlayer.Text = "Player 1";
            else
                displayPlayer.Text = "Player 2";
        }

        
        public void checkIt()
        {
            if (button1.Text != "" & button2.Text != "" & button3.Text != "")
            {
                if (button1.Text == button2.Text && button1.Text == button3.Text)
                {
                    button1.BackColor = Color.Green;
                    button1.ForeColor = Color.White;
                    button2.BackColor = Color.Green;
                    button2.ForeColor = Color.White;
                    button3.BackColor = Color.Green;
                    button3.ForeColor = Color.White;
                    if (button1.Text == "X")
                    {
                        MessageBox.Show("Player 1 Wins!");
                        score1++;
                        player1Score.Text = score1.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Player 2 Wins!");
                        score2++;
                        player2Score.Text = score2.ToString();
                    }
                    //cleargame();
                }
            }
            if (button1.Text != "" & button5.Text != "" & button9.Text != "")
            {
                if (button1.Text == button5.Text && button1.Text == button9.Text)
                {
                    button1.BackColor = Color.Green;
                    button1.ForeColor = Color.White;
                    button5.BackColor = Color.Green;
                    button5.ForeColor = Color.White;
                    button9.BackColor = Color.Green;
                    button9.ForeColor = Color.White;
                    if (button1.Text == "X")
                    {
                        MessageBox.Show("Player 1 Wins!");
                        //player1score.Text = player1.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Player 2 Wins!");
                        //player2score.Text = player2.ToString();
                    }
                }
            }

            if (button3.Text != "" & button5.Text != "" & button7.Text != "")
            {
                if (button3.Text == button5.Text && button3.Text == button7.Text)
                {
                    button3.BackColor = Color.Green;
                    button3.ForeColor = Color.White;
                    button5.BackColor = Color.Green;
                    button5.ForeColor = Color.White;
                    button7.BackColor = Color.Green;
                    button7.ForeColor = Color.White;
                    if (button3.Text == "X")
                    {
                        MessageBox.Show("Player 1 Wins!");
                        //player1score.Text = player1.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Player 2 Wins!");
                        //player2score.Text = player2.ToString();
                    }
                }
            }

            if (button3.Text != "" & button6.Text != "" & button9.Text != "")
            {
                if (button3.Text == button6.Text && button3.Text == button9.Text)
                {
                    button3.BackColor = Color.Green;
                    button3.ForeColor = Color.White;
                    button6.BackColor = Color.Green;
                    button6.ForeColor = Color.White;
                    button9.BackColor = Color.Green;
                    button9.ForeColor = Color.White;
                    if (button3.Text == "X")
                    {
                        MessageBox.Show("Player 1 Wins!");
                        //player1score.Text = player1.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Player 2 Wins!");
                        //player2score.Text = player2.ToString();
                    }
                }
            }

            if (button7.Text != "" & button8.Text != "" & button9.Text != "")
            {
                if (button7.Text == button8.Text && button7.Text == button9.Text)
                {
                    button7.BackColor = Color.Green;
                    button7.ForeColor = Color.White;
                    button8.BackColor = Color.Green;
                    button8.ForeColor = Color.White;
                    button9.BackColor = Color.Green;
                    button9.ForeColor = Color.White;
                    if (button7.Text == "X")
                    {
                        MessageBox.Show("Player 1 Wins!");
                        //player1score.Text = player1.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Player 2 Wins!");
                        //player2score.Text = player2.ToString();
                    }
                }
            }

            if (button1.Text != "" & button4.Text != "" & button7.Text != "")
            {
                if (button1.Text == button4.Text && button1.Text == button7.Text)
                {
                    button1.BackColor = Color.Green;
                    button1.ForeColor = Color.White;
                    button4.BackColor = Color.Green;
                    button4.ForeColor = Color.White;
                    button7.BackColor = Color.Green;
                    button7.ForeColor = Color.White;
                    if (button1.Text == "X")
                    {
                        MessageBox.Show("Player 1 Wins!");
                        //player1score.Text = player1.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Player 2 Wins!");
                        //player2score.Text = player2.ToString();
                    }
                }
            }

            if (button2.Text != "" & button5.Text != "" & button8.Text != "")
            {
                if (button2.Text == button5.Text && button2.Text == button8.Text)
                {
                    button2.BackColor = Color.Green;
                    button2.ForeColor = Color.White;
                    button5.BackColor = Color.Green;
                    button5.ForeColor = Color.White;
                    button8.BackColor = Color.Green;
                    button8.ForeColor = Color.White;
                    if (button2.Text == "X")
                    {
                        MessageBox.Show("Player 1 Wins!");
                        //player1score.Text = player1.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Player 2 Wins!");
                        //player2score.Text = player2.ToString();
                    }
                }
            }

        }

        private void playAgain_Click(object sender, EventArgs e)
        {
            button1.Text = "";
            button2.Text = "";
            button3.Text = "";
            button4.Text = "";
            button5.Text = "";
            button6.Text = "";
            button7.Text = "";
            button8.Text = "";
            button9.Text = "";

            player = 1;

            button1.BackColor = Color.Empty;
            button1.ForeColor = Color.Empty;

            button2.BackColor = Color.Empty;
            button2.ForeColor = Color.Empty;

            button3.BackColor = Color.Empty;
            button3.ForeColor = Color.Empty;

            button4.BackColor = Color.Empty;
            button4.ForeColor = Color.Empty;

            button5.BackColor = Color.Empty;
            button5.ForeColor = Color.Empty;

            button6.BackColor = Color.Empty;
            button6.ForeColor = Color.Empty;

            button7.BackColor = Color.Empty;
            button7.ForeColor = Color.Empty;

            button8.BackColor = Color.Empty;
            button8.ForeColor = Color.Empty;

            button9.BackColor = Color.Empty;
            button9.ForeColor = Color.Empty;
        }
    }
}