﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Spiel_Server
{
    class Server
    {
        static TcpListener listener;
        static int player = 0;
        static int count = 0;

        const int LIMIT = 2;

        static List<Socket> sockets = new List<Socket>();

        static void Main(string[] args)
        {
            listener = new TcpListener(IPAddress.Any, 2055);
            listener.Start();

            Console.WriteLine("Server launching...");

            for (int i = 0; i < LIMIT; i++)
            {
                Thread t = new Thread(new ThreadStart(Service));
                t.Start();
            }
        }

        public static void Service()
        {
                try
                {
                    int index;

                    sockets.Add(listener.AcceptSocket());
                    index = sockets.Count - 1;

                    Console.WriteLine("Player " + index + " connected");

                    Stream s = new NetworkStream(sockets[index]);
                    StreamReader sr = new StreamReader(s);
                    
                    while(sockets.Count != LIMIT);
                    StreamWriter sw = new StreamWriter(new NetworkStream(sockets[index == 0 ? 1 : 0]));
                    sw.AutoFlush = true;

                    sw.WriteLine("Player " + index);

                    count++;

                    while(true)
                    {
                        var received = sr.ReadLine();
                        Console.WriteLine("Received: " + received);

                        var split = received.Split('/');
                        var split_player = split[0];
                        var split_label = split[1];

                    
                        var player_int = Convert.ToInt32(split_player);
                        if (player_int == 0)
                            player = 1;
                        if (player_int == 1)
                            player = 0;

                        string answer = player.ToString() + "/" + split_label;

                        sw.WriteLine(answer);
                        Console.WriteLine(answer);
                    }
                    Console.ReadLine();
                    //s.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                //Console.WriteLine("Player disconnected");
                //soc.Close();
            }
        
    }
}
