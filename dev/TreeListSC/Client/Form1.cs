﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;

namespace Client
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void sendRequestButton_Click(object sender, EventArgs e)
        {
            XDocument xd = new XDocument();

            TcpClient client = new TcpClient("127.0.0.1", 2055);

            try
            {
                Stream stream = client.GetStream();

                StreamReader reader = new StreamReader(stream);
                StreamWriter writer = new StreamWriter(stream);

                writer.AutoFlush = true;
                writer.WriteLine("DIR|" + textBox1.Text);

                TreeNode node = new TreeNode();
                string dirs = reader.ReadToEnd();

                XElement elem = XElement.Parse(dirs);
                
                node = ToTreeNode(elem);
                node.Expand();
                treeView1.Nodes.Add(node);



                XElement element = xd.XPathSelectElement(dirs);

                listView1.Items.Clear();
                listView1.View = View.Details;

                if (element == null) return;
                if (element.Name == "Dir")
                    foreach (var item in element.Elements())
                    {
                        if (item.Name == "Dir") continue;
                        if (item.HasAttributes)
                        {
                            var a = item.Attributes();

                            ListView lvi = null;

                            foreach (var att in a)
                            {
                                if (lvi == null)
                                    lvi = listView1.Items.Add(att.Value.ToString());
                                else
                                    lvi.SubItems.Add(att.Value.ToString());
                            }
                        }
                    }

            }
            catch (Exception)
            {
            }
            
        }

        public static TreeNode ToTreeNode(XElement elem)
        {
            return new TreeNode(elem.Attribute("Name").Value,
                                    elem.Elements().Select(e => ToTreeNode(e)).ToArray());
        }

        public static ListViewItem ToListViewItem(XElement elem)
        {
            return new ListViewItem(new string[] {
                elem.Attribute("Name").Value,
                elem.Attribute("Length").Value,
                elem.Attribute("LastWriteTime").Value
            });
        }

        private void fileTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            listView1.Items.Clear();


            TreeNode node = treeView1.SelectedNode;
            //string path = @"" + e.Node.FullPath;
            string path = textBox1.Text + @"\..\";
            path += node.FullPath;

            MessageBox.Show(node.FullPath);

            TcpClient client = new TcpClient("127.0.0.1", 2055);

            //try
            //{
            //    Stream stream = client.GetStream();

            //    StreamReader reader = new StreamReader(stream);
            //    StreamWriter writer = new StreamWriter(stream);

            //    writer.AutoFlush = true;
            //    writer.WriteLine("FILE|" + path);

            //    TreeNode fileNode = new TreeNode();
            //    string files = reader.ReadToEnd();

            //    XElement elem = XElement.Parse(files);

            //    foreach (XElement child in elem.Elements())
            //        listView1.Items.Add(ToListViewItem(child));
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Exception: \n" + ex);
            //}
        }
    }
}
